# vue-template

This template includes:

*  Vue.js application template
*  Linting using TSLint and the Vue builders
*  Automatic releases using semantic-release (ESLint Code Convention) and Gitlab CI / CD
*  Automatic Unit tests using Mocha/Chai and the Vue cli
*  Automatic documentation publishing using Gitlab CI / CD and a self written script which puts the docs in the docs folder to the wiki
*  For public registry: Publishing of packages, for usage add the following lines to package.json, add the npm publish module in .releaserc and provide a   valid NPM token:
```
"publishConfig": {
    "access": "public",
    "registry": "https://registry.npmjs.org/",
    "tag": "latest"
}
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Commit convention
See [Commit convention](ESLintConvention) for the commit convention.